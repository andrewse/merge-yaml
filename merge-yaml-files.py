#!/usr/bin/env python3
import sys

import io
import yaml


def load_yaml_into_dict(filename):
    with io.open(filename, 'r') as yaml_file:
        return yaml.load(yaml_file)


def main():
    file1 = load_yaml_into_dict('file1.yaml')
    print('File 1 contents: %s' % file1)
    file2 = load_yaml_into_dict('file2.yaml')
    print('File 2 contents: %s' % file1)

    file1.update(file2)
    print('File 1 contents updated: %s' % file1)

    with io.open('merged.yaml', 'w') as merged_file:
        yaml.dump(file1, merged_file, default_flow_style=False)

    with io.open('merged.yaml', 'r') as merged_file:
        for line in merged_file.readlines():
            print(line, end='')


if __name__ == "__main__":
    main()
    sys.exit(0)