.PHONY: clean clean-test clean-pyc clean-build docs help build-deps build
.DEFAULT_GOAL := help
define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

help: ## Generic help function that dumps out all the target lines with help into
	@cat < $(MAKEFILE_LIST) \
		| awk -F':' '/^[a-zA-Z0-9][^$$#\/\t=]*:([^=]|$$)/ { split($$2, A, "##"); print $$1"," A[2]; }' \
		| column -t -s','

clean: clean-build clean-pyc clean-test ## Remove everything

run: setup
	@( \
		pipenv shell; \
		./merge-yaml-files.py; \
	)

setup:
	pipenv install
